from ml4proflow.modules import DataFlowManager
from ml4proflow_mods.io.modules import FileSourceModule, FileSinkModule

options = {}

for ofile in ['10M.json', '10M.h5']:

    dfm = DataFlowManager()
    src = FileSourceModule(dfm, {'channels_push': 'src',
                                 'file':'10M.csv'})
    if 'h5' in ofile:
        options['key'] = 'a1'
    dst = FileSinkModule(dfm, {'file': ofile,
                               'channels_pull': 'src',
                               'parser_options': options})
    src.execute_once()
