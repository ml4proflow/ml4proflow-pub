import glob
import sys
import json
import os
import matplotlib
import matplotlib.pyplot as plt
import pandas
import numpy as np
import ast
from more_itertools import pairwise  # only for <3.10 ;; 3.10 --> itertools
import argparse
import logging


def is_float(element):
    try:
        float(element)
        return True
    except ValueError:
        return False


logging.basicConfig(#filename = "logfile.log",
                    stream=sys.stdout,
                    level=logging.INFO)
logger = logging.getLogger()

parser = argparse.ArgumentParser(description='Plotting')
parser.add_argument('--font-size', default=8)
parser.add_argument('--data-glob', default='*.json')
parser.add_argument('--config-dir', default='../')
parser.add_argument('--parser-ignore-host', default=[], nargs='*')

args = parser.parse_args()  # arguments

plt.rcParams.update({'font.size': args.font_size})
files = glob.glob(args.data_glob)

stat_funcs = ['mean', 'count', 'max', 'min']  # TODO: move to args

node2label = {'node_101': 'AMD Epic Gen1',
              'node_102': "JETSON AGX",
              'node_103': 'Xeon D-1577',
              'node_104': 'Xeon E2176M',
              'node_106': 'Intel i7-4700EQ',
              'node_155': "JETSON TX2",
              'node_156': "JETSON TX2 (2)",
              'node_159': "JETSON TX2 (3)",
              'node_222': 'Intel i7-7700K'
              }

plot_data = pandas.read_pickle('parsed_data')

print(plot_data.keys())

passthrough_all = plot_data[plot_data.config.str.contains("passthrough")]
passthrough_mt  = passthrough_all[passthrough_all.config.str.contains("mt")]
passthrough_single = passthrough_all[passthrough_all.config.str.contains("mt")==False]

plot = passthrough_single.set_index(['host', 'config'])
plot = plot[['module_runs_k_per_s', 'runs_per_s_per_w']].sort_values("module_runs_k_per_s")
print(plot)
plot = plot.plot(kind='bar', secondary_y=['runs_per_s_per_w'])
plot.set_ylabel('Runs per second in k/s')
plot.set_title('Passthrough') #Process 1K elements on a 1K long pipeline
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('passthrough_single_overview.pdf', dpi=300)

passthrough_single_real = passthrough_single[passthrough_all.config.str.contains("_Repeating")]
plot = passthrough_single_real.set_index(['host'])
plot = plot[['module_runs_k_per_s', 'runs_per_s_per_w']].sort_values("module_runs_k_per_s")
print(plot)
plot = plot.plot(kind='bar', secondary_y=['runs_per_s_per_w'])
plot.set_ylabel('Runs per second in k/s')
plot.right_ax.set_ylabel('Efficiency in runs/watt')
plot.set_title('Passthrough') #Process 1K elements on a 1K long pipeline
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('passthrough_single_real.pdf', dpi=300)

plot = passthrough_all[passthrough_all.config.str.contains("Fake")]
max_runs_per_host_idx = plot.groupby('host')['module_runs_k_per_s'].idxmax()
min_latency_per_host_singlecore_idx = plot[plot['config'].str.contains("mt")==False]  # plot.groupby('host')['data_process_times_mean'].transform(min) == plot['data_process_times_mean']
multicore_runs = plot[plot['config'].str.contains("mt")]
min_latency_per_host_multicore_only_idx = multicore_runs.groupby('host')[
    'process_latency_mean'].idxmin()
print(min_latency_per_host_singlecore_idx)

tmp = {'module_runs_k_per_s': {}, 'config': {}, 'process_latency_mean': {}}

for host in plot['host']:

    #    tmp[(host, 'singlecore')] = {}
    #    tmp[(host, 'multicore')] = {}
    #    tmp[(host, 'max_perf')] = {}
    for c in ['module_runs_k_per_s', 'config', 'process_latency_mean']:
        print(min_latency_per_host_singlecore_idx[
                  min_latency_per_host_singlecore_idx['host'] == host][c])
        try:
            tmp[c][(host, 'Singlecore')] = min_latency_per_host_singlecore_idx[
                min_latency_per_host_singlecore_idx['host'] == host][c].values[
                0]
        except:
            pass
        try:
            tmp[c][(host, 'MC Latency')] = plot[c][
                min_latency_per_host_multicore_only_idx[host]]
        except:
            pass
        tmp[c][(host, 'MC Performance')] = plot[c][max_runs_per_host_idx[host]]
print(tmp)
tmp = pandas.DataFrame(tmp)
print(tmp)
plot = tmp.plot(kind='bar', secondary_y=[
    'process_latency_mean'])  # , stacked=True x=['config','host'],
# ax1 = plot.twinx()
# ax1.set_ylabel('Latency in s')
plot.right_ax.set_ylabel('Latency in ms')
plot.set_ylabel('Throughput in 1/s')
# plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.get_legend().remove()
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_test.pdf', dpi=300)



























plot = plot_data[plot_data['config'].str.contains("dummy")]
print(plot)
plot = plot.set_index(['host', 'config'])
plot = plot[['module_runs_k_per_s', 'actualPowerUsage_mean',
             'runs_per_s_per_w']].sort_values("module_runs_k_per_s").plot(
    kind='bar')  # , stacked=True
plot.set_ylabel('Time in s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_through_dummies.pdf',
                          dpi=300)

plot = plot_data[plot_data['config'].str.contains("dummy")]
print(plot)
plot = plot.groupby('host')['runs_per_s_per_w'].max()
# plot = plot[plot['runs_per_s_per_w']==max_eff]
# plot = plot.set_index('host')
print(plot)
plot = plot.sort_values().plot(kind='bar')  # , stacked=True
plot.set_ylabel('Throughput per Watt in 1/s/w')
plot.set_title('')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.get_legend().remove()
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_eff.pdf', dpi=300)

plot = plot_data[plot_data['config'].str.contains("dummy")]
plot = plot.set_index(['host', 'config'])
plot = plot[['module_runs_k_per_s', 'data_process_times_mean']].sort_values(
    "module_runs_k_per_s").plot(kind='bar', secondary_y=[
    'data_process_times_mean'])  # , stacked=True
plot.set_ylabel('Time in s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_ThrouLat.pdf', dpi=300)

plot = plot_data[plot_data['config'].str.contains("dummy")]
max_runs_per_host_idx = plot.groupby('host')['module_runs_k_per_s'].idxmax()
min_latency_per_host_singlecore_idx = plot[plot['config'].str.contains(
    "0buf")]  # plot.groupby('host')['data_process_times_mean'].transform(min) == plot['data_process_times_mean']
multicore_runs = plot[~plot['config'].str.contains("0buf")]
min_latency_per_host_multicore_only_idx = multicore_runs.groupby('host')[
    'data_process_times_mean'].idxmin()
print(min_latency_per_host_singlecore_idx)

tmp = {'module_runs_k_per_s': {}, 'config': {}, 'data_process_times_mean': {}}

for host in plot['host']:

    #    tmp[(host, 'singlecore')] = {}
    #    tmp[(host, 'multicore')] = {}
    #    tmp[(host, 'max_perf')] = {}
    for c in ['module_runs_k_per_s', 'config', 'data_process_times_mean']:
        print(min_latency_per_host_singlecore_idx[
                  min_latency_per_host_singlecore_idx['host'] == host][c])
        try:
            tmp[c][(host, 'Singlecore')] = min_latency_per_host_singlecore_idx[
                min_latency_per_host_singlecore_idx['host'] == host][c].values[
                0]
        except:
            pass
        try:
            tmp[c][(host, 'MC Latency')] = plot[c][
                min_latency_per_host_multicore_only_idx[host]]
        except:
            pass
        tmp[c][(host, 'MC Performance')] = plot[c][max_runs_per_host_idx[host]]
print(tmp)
tmp = pandas.DataFrame(tmp)
print(tmp)
plot = tmp.plot(kind='bar', secondary_y=[
    'data_process_times_mean'])  # , stacked=True x=['config','host'],
# ax1 = plot.twinx()
# ax1.set_ylabel('Latency in s')
plot.right_ax.set_ylabel('Latency in ms')
plot.set_ylabel('Throughput in 1/s')
# plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.get_legend().remove()
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_test.pdf', dpi=300)

sys.exit(1)

# for host in plot['host']:
#    tmp[(host, 'max_perf')] = {}
#    tmp[(host, 'single_core')] = {}
#    tmp[(host, 'multicore_core')] = {}
#
#    for c in ['module_runs_k_per_s', 'config', 'data_process_times_mean']:
#        tmp[(host, 'max_perf')][c] = plot[c][max_runs_per_host_idx[host]]
#        tmp[(host, 'single_core')][c] = min_latency_per_host_singlecore_idx[c][0]
#        try: #REMOVE
#            tmp[(host, 'multicore_core')][c] = plot[c][min_latency_per_host_multicore_only_idx[host]]
#        except:
#            pass
tmp = {}

for c in ['module_runs_k_per_s', 'config', 'data_process_times_mean']:
    tmp[(c, 'singlecore')] = {}
    tmp[(c, 'multicore')] = {}
    tmp[(c, 'max_perf')] = {}
    for host in plot['host']:
        print(min_latency_per_host_singlecore_idx[
                  min_latency_per_host_singlecore_idx['host'] == host][c])
        try:
            tmp[(c, 'singlecore')][host] = min_latency_per_host_singlecore_idx[
                min_latency_per_host_singlecore_idx['host'] == host][c].values[
                0]
        except:
            pass
        try:
            tmp[(c, 'multicore')][host] = plot[c][
                min_latency_per_host_multicore_only_idx[host]]
        except:
            pass
        tmp[(c, 'max_perf')][host] = plot[c][max_runs_per_host_idx[host]]
print(tmp)
tmp = pandas.DataFrame(tmp)
print(tmp)
plot = tmp.plot(kind='bar',
                secondary_y=[('data_process_times_mean', x) for x in
                             ['max_perf', 'singlecore',
                              'multicore']])  # , stacked=True
# ax1 = plot.twinx()
# ax1.set_ylabel('Latency in s')
plot.right_ax.set_ylabel('Latency in s')
plot.set_ylabel('Runs per Second')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_test.pdf', dpi=300)

sys.exit(-1)
# plot = plot.set_index(['host','config'])
plot = plot[['module_runs_k_per_s', 'data_process_times_mean']].sort_values(
    "module_runs_k_per_s").plot(kind='bar', secondary_y=[
    'data_process_times_mean'])  # , stacked=True
plot.set_ylabel('Time in s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18, 10)  # (4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_ThrouLat.pdf', dpi=300)

sys.exit(-1)
fig, ax = plt.subplots()
ax3 = ax.twinx()

plot = plot_data[plot_data['config'].str.contains("dummy")]
print(plot)
plot = plot.set_index(['host', 'config'])
test = plot.groupby('host').max()
print(test)
sys.exit(1)
plot = plot[['module_runs_k_per_s', 'data_process_times_mean']].sort_values(
    "module_runs_k_per_s")
plot['module_runs_k_per_s'].plot(ax=ax,
                                 kind='bar')  # , stacked=True #, 'actualPowerUsage_mean','runs_per_s_per_w'
plot['data_process_times_mean'].plot(ax=ax3,
                                     kind='bar')  # , stacked=True #, 'actualPowerUsage_mean','runs_per_s_per_w'
ax.set_ylabel('Time in s')
# fig.set_title('Process 1K elements on a 1K long pipeline')
fig.set_size_inches(18, 10)  # (4.1, 3)
# fig.grid(axis='y')
# fig.set_axisbelow(True)
fig.tight_layout()
fig.savefig('processing_time_1k_pass_ThrouLat.pdf', dpi=300)

# plot = plot_data[plot_data['config']=="1K-PassThrough"]
# plot = plot.set_index('label')
plot = plot[['timing_json', 'timing_dfg', 'timing_process']].sort_values(
    "timing_process").plot(kind='bar', stacked=True)
plot.set_ylabel('Time in s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_through.pdf', dpi=300)

plot = plot_data[plot_data['config'] == "1K-PassThrough"]
plot = plot.set_index('label')
plot = plot[['module_runs_per_s']].sort_values("module_runs_per_s").plot(
    kind='bar', stacked=True)
plot.set_ylabel('Calls per s')
plot.set_xlabel('Host')
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.get_legend().remove()
# plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(4.1, 3)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_perf_1k_pass_through.pdf', dpi=300)

plot = plot_data.set_index(['label', 'config'])
plot = plot[['module_runs_per_s']].sort_values("module_runs_per_s").plot(
    kind='bar')
plot.set_ylabel('Items? * Modules / s in #K/s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18.5, 10.5)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_perf_1k_pass_through_all.pdf', dpi=300)

idx_max = plot_data.groupby(by='label')['module_runs_per_s'].transform(max) == \
          plot_data['module_runs_per_s']
plot = plot_data[idx_max]
plot['l'] = plot['label'] + '\n' + plot['config']
plot = plot.set_index('l')  # ['label','config'])

plot = plot[['module_runs_per_s']].sort_values("module_runs_per_s").plot(
    kind='bar')
print(plot)
# plot.set_ylabel('Items? * Modules / s in #K/s')
plot.set_ylabel('Calls per s')
plot.grid(axis='y')
plot.set_axisbelow(True)
plot.set_xlabel('Host + Config')
# plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_legend().remove()
plot.get_figure().set_size_inches(4.1, 3)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_perf_1k_pass_through_multi.pdf', dpi=300)

sys.exit(0)

print(plot_data['labels'])
fig, ax = plt.subplots()
ax.bar(plot_data['label'], plot_data['timing_json'], label="JSON parse time")
ax.bar(plot_data['label'], plot_data['timing_dfg'], label="JSON dfg time")
ax.bar(plot_data['label'], plot_data['timing_process'],
       label="JSON process time")
ax.set_ylabel('Time in s')
ax.set_title('Process 1K elements on a 1K long pipeline')
ax.legend()
fig.set_size_inches(18.5, 10.5)
fig.savefig('processing_time_1k_pass_through.pdf', dpi=300)

fig, ax = plt.subplots()
ax.bar(plot_data['label'], plot_data['module_runs_per_s'], label="modules/s")
ax.set_ylabel('Items? * Modules / s in #K/s')
ax.set_title('Process 1K elements on a 1K long pipeline')
ax.legend()
fig.set_size_inches(18.5, 10.5)
fig.savefig('processing_perf_1k_pass_through.pdf', dpi=300)


































parser = argparse.ArgumentParser(description='Plotting')
parser.add_argument('--font-size', default=8)
parser.add_argument('--data-glob', default='*.json')
args = parser.parse_args(arguments)

plt.rcParams.update({'font.size': args.font_size})


node2label = {'node_101': 'epic',
              'node_102': "JETSON AGX",
              'node_103': 'Xeon D-1577',
              'node_104': 'Xeon E2176M',
              'node_106': 'Intel i7-4700EQ',
              'node_155': "JETSON TX2",
              'node_156': "JETSON TX2 (2)",
              'node_222': 'Intel i7-7700K'
              }

# Read all data
plot_data = pandas.DataFrame()
for file in files:
    with open(file) as f:
        data = json.load(f)
    start_time = float(data['timing_data']['python_init_done'])
    json_time = float(data['timing_data']['python_json_done'])
    dfg_time = float(data['timing_data']['python_dfg_create_done'])
    process_time = float(data['timing_data']['python_process_done'])
    plot_data = plot_data.append({'label': node2label[data['host']],
                                  'config': data['run'].split('.')[0],
                                  'timing_json': json_time-start_time,
                                  'timing_dfg': dfg_time-json_time,
                                  'timing_process': process_time-dfg_time,
                                  'module_runs_per_s': 1000*1000/(process_time-dfg_time)/1000}, ignore_index=True)
print(plot_data)

plot = plot_data[plot_data['config']=="1K-PassThrough"]
plot = plot.set_index('label')
plot = plot[['timing_json', 'timing_dfg', 'timing_process']].sort_values("timing_process").plot(kind='bar', stacked=True)
plot.set_ylabel('Time in s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(4.1, 3)
plot.grid(axis='y')
plot.set_axisbelow(True) 
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_time_1k_pass_through.pdf', dpi=300)

plot = plot_data[plot_data['config']=="1K-PassThrough"]
plot = plot.set_index('label')
plot = plot[['module_runs_per_s']].sort_values("module_runs_per_s").plot(kind='bar', stacked=True)
plot.set_ylabel('Calls per s')
plot.set_xlabel('Host')
plot.grid(axis='y')
plot.set_axisbelow(True) 
plot.get_legend().remove()
#plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(4.1, 3)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_perf_1k_pass_through.pdf', dpi=300)

plot = plot_data.set_index(['label','config'])
plot = plot[['module_runs_per_s']].sort_values("module_runs_per_s").plot(kind='bar')
plot.set_ylabel('Items? * Modules / s in #K/s')
plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_figure().set_size_inches(18.5, 10.5)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_perf_1k_pass_through_all.pdf', dpi=300)

idx_max = plot_data.groupby(by='label')['module_runs_per_s'].transform(max) == plot_data['module_runs_per_s']
plot = plot_data[idx_max]
plot['l'] = plot['label']+'\n'+plot['config']
plot = plot.set_index('l') #['label','config'])

plot = plot[['module_runs_per_s']].sort_values("module_runs_per_s").plot(kind='bar')
print(plot)
#plot.set_ylabel('Items? * Modules / s in #K/s')
plot.set_ylabel('Calls per s')
plot.grid(axis='y')
plot.set_axisbelow(True) 
plot.set_xlabel('Host + Config')
#plot.set_title('Process 1K elements on a 1K long pipeline')
plot.get_legend().remove()
plot.get_figure().set_size_inches(4.1, 3)
plot.get_figure().tight_layout()
plot.get_figure().savefig('processing_perf_1k_pass_through_multi.pdf', dpi=300)


sys.exit(0)



print(plot_data['labels'])
fig, ax = plt.subplots()
ax.bar(plot_data['label'], plot_data['timing_json'], label="JSON parse time")
ax.bar(plot_data['label'], plot_data['timing_dfg'], label="JSON dfg time")
ax.bar(plot_data['label'], plot_data['timing_process'], label="JSON process time")
ax.set_ylabel('Time in s')
ax.set_title('Process 1K elements on a 1K long pipeline')
ax.legend()
fig.set_size_inches(18.5, 10.5)
fig.savefig('processing_time_1k_pass_through.pdf', dpi=300)

fig, ax = plt.subplots()
ax.bar(plot_data['label'], plot_data['module_runs_per_s'], label="modules/s")
ax.set_ylabel('Items? * Modules / s in #K/s')
ax.set_title('Process 1K elements on a 1K long pipeline')
ax.legend()
fig.set_size_inches(18.5, 10.5)
fig.savefig('processing_perf_1k_pass_through.pdf', dpi=300)