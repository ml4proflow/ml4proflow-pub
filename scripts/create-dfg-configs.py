#!/usr/bin/env python3
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os
import glob
import sys
import json
from os.path import normpath, realpath, basename, dirname
from ml4proflow import modules, modules_extra
from ml4proflow.module_finder import find_by_name
from ml4proflow_mods.io import modules as io_modules


#statistics?
def make_passthrough_config(config_path: str, src: str, src_config: dict,
                            depth: int) -> None:
    dfm = modules.DataFlowManager()
    dfg = modules.DataFlowGraph(dfm, {})
    src_class = find_by_name(src)
    src_config['channels_push'] = ['chan0']
    src_inst = src_class(dfm=dfm, config=src_config)
    dfg.add_module(src_inst)
    for i in range(depth):
        config = {'channels_pull': [f'chan{i}'],
                  'channels_push': [f'chan{i+1}']}
        m = modules_extra.PassThroughModule(dfm=dfm, config=config)
        dfg.add_module(m)
    config = {'channels_pull': [f'chan{depth}']}
    m = modules_extra.StatisticSinkModule(dfm=dfm, config=config)
    dfg.add_module(m)
    src_name = src.rsplit('.')[-1]
    with open(f'{config_path}/passthrough_{src_name}_{depth}.json',
              mode='w') as jf:
        json.dump(dfg.get_config(), jf)


def make_passthrough_config_mt(config_path: str, src: str, src_config: dict,
                               depth: int,
                               max_queue_size: int, proc_count: int) -> None:
    dfm = modules.DataFlowManager()
    push_channels = ['sub_src']
    dfg_sub_desc = []
    for i in range(depth):
        pull_channels = push_channels
        push_channels = ['pass_through_%d' % i]
        dfg_sub_desc.append({
            "module_ident": "ml4proflow.modules_extra.PassThroughModule",
            "module_config": {
                'channels_push': push_channels,
                'channels_pull': pull_channels
            }
        })
    dfg_sub_desc.append({
        "module_ident": "ml4proflow.modules_extra.StatisticSinkModule",
        "module_config": {'channels_pull': push_channels}
    })
    src_config['channels_push'] = ['src']
    dfg_sub_desc = {'modules': dfg_sub_desc}
    dfg = {'modules': [{
        "module_ident": src,
        "module_config": src_config
    }, {
        "module_ident": "ml4proflow_mods.multiprocessing"
                        + ".modules.DFGProcessSinkModule",
        "module_config": {'max_queue_size': max_queue_size,
                          'process_count': proc_count,
                          'channels_pull': ['src'],
                          'subgraph_channel': 'sub_src',
                          'subgraph_config': dfg_sub_desc,
                          },
    }]}
    dut_dfg = modules.DataFlowGraph(dfm, dfg)
    src_name = src.rsplit('.')[-1]
    with open(f'{config_path}/passthrough_mt_{src_name}_{depth}_{max_queue_size}_{proc_count}.json',
              mode='w') as f:
        json.dump(dut_dfg.get_config(), f)
    dut_dfg.shutdown()

#statistics?
def make_freader_config(config_path: str, ifile: str, engine: str) -> None:
    dfm = modules.DataFlowManager()
    dfg = modules.DataFlowGraph(dfm, {})
    csv_name, csv_ext = basename(ifile).split('.', maxsplit=1)
    config = {"file": ifile,
              "parser_options": {"engine": engine},
              "read_metadata": False,
              "channels_push": ['chan0']}
    m1 = io_modules.FileSourceModule(dfm, config)
    dfg.add_module(m1)
    fe_mode = csv_ext.replace('.', '_')
    with open(f'{config_path}/csv_{csv_name}_engine_{engine}_{fe_mode}.json',
              mode='w') as jf:
        json.dump(dfg.get_config(), jf)


def make_gen_freader_config(config_path: str, ifile: str) -> None:
    dfm = modules.DataFlowManager()
    dfg = modules.DataFlowGraph(dfm, {})
    name, ext = basename(ifile).split('.', maxsplit=1)
    config = {"file": ifile,
              "parser_options": {},
              "read_metadata": False,
              "channels_push": ['chan0']}
    m1 = io_modules.FileSourceModule(dfm, config)
    dfg.add_module(m1)
    with open(f'{config_path}/{ext}_{name}.json',
              mode='w') as jf:
        json.dump(dfg.get_config(), jf)


def main(arguments: list[str] = sys.argv[1:]) -> None:
    default_root_path = dirname(realpath(__file__))
    default_full_root_path = normpath(f'{default_root_path}/../')

    parser = ArgumentParser(description='Create DFG configs',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--paths-rel-to', default=default_full_root_path,
                        help='Paths in configs are stored rel to this value')
    parser.add_argument('--rel-data-path', default='./data/',
                        help='Rel to paths-rel-to')
    parser.add_argument('--csv-engines', default=['c', 'pyarrow', 'python'],
                        nargs='+')
    parser.add_argument('--csv-glob', default='*.csv*',
                        help='rel to data dir')
    parser.add_argument('--json-glob', default='*.json*',
                        help='rel to data dir')
    parser.add_argument('--h5-glob', default='*.h5*',
                        help='rel to data dir')
    parser.add_argument('--config-path', default='./configs/')
    parser.add_argument('--write-csv-configs', action='store_true')
    parser.add_argument('--write-json-configs', action='store_true')
    parser.add_argument('--write-h5-configs', action='store_true')
    parser.add_argument('--write-single-pt-configs', action='store_true',
                        help='PT = Passthrough')
    parser.add_argument('--write-multi-pt-configs', action='store_true',
                        help='Multicore Passthrough')
    parser.add_argument('--pt-depth', default=2000)
    parser.add_argument('--pt-source', nargs='+',
                        default=['ml4proflow.modules_extra.FakeRepeatingSourceModule',
                                 'ml4proflow.modules_extra.RepeatingSourceModule'])
    parser.add_argument('--multi-pt-max-queue-size', nargs='+',
                        default=[1, 4, 8, 16, 32, 64])
    parser.add_argument('--multi-pt-proc-count', nargs='+',
                        default=[1, 3, 7, 15, 31, 63])

    args = parser.parse_args(arguments)
    os.chdir(args.paths_rel_to)
    if args.write_csv_configs:
        rel_csv_files = glob.glob(f'{args.rel_data_path}/{args.csv_glob}')
        print(f'Found {len(rel_csv_files)} CSV files')
        for rel_csv_file in rel_csv_files:
            for csv_engine in args.csv_engines:
                make_freader_config(args.config_path, rel_csv_file, csv_engine)
    if args.write_json_configs:
        rel_json_files = glob.glob(f'{args.rel_data_path}/{args.json_glob}')
        print(f'Found {len(rel_json_files)} JSON files')
        for rel_json_file in rel_json_files:
            make_gen_freader_config(args.config_path, rel_json_file)

    if args.write_h5_configs:
        rel_h5_files = glob.glob(f'{args.rel_data_path}/{args.h5_glob}')
        print(f'Found {len(rel_h5_files)} H5 files')
        for rel_h5_file in rel_h5_files:
            make_gen_freader_config(args.config_path, rel_h5_file)

    if args.write_single_pt_configs:
        for src in args.pt_source:
            make_passthrough_config(args.config_path, src, {}, args.pt_depth)

    if args.write_multi_pt_configs:
        for src in args.pt_source:
            for max_queue_size in args.multi_pt_max_queue_size:
                for proc_count in args.multi_pt_proc_count:
                    make_passthrough_config_mt(args.config_path, src, {},
                                               args.pt_depth,
                                               max_queue_size,
                                               proc_count)


if __name__ == '__main__':
    main(sys.argv[1:])
